# Pygwinc MyIFO


This is an example repository for building your own interferometer that hooks into GWINC's discovery feature. This uses modern python namespace packages.

This will add an interferometer MyIFO to GWINC's list of interferometer. This requires https://git.ligo.org/gwinc/pygwinc/-/merge_requests/167 to be merged into GWINC.

This MyIFO is just aLIGO, but uses an old version of the quantum code to keep the BnC model alive.

The MyIFO inteferometer will become available if this package is installed. Since this is an 
example, it should usually only be installed in "development" mode. This can be done using

`pip install -e .` 
if you have install privledges as within a conda envronment or root acces, otherwise add the --user 
flag before -e.

## Notable locations

unlike GWINC, this package uses the 'src' layout of python. This slightly less convenient, burying 
the code one directory deeper, but the installation is principally out of src/ and thus more clear. 

The setup.cfg is configured to use find_namespace: and where=src inside of its setup.cfg file.

To add the interferometer, put any code into packages inside of the src/ directory. In this case, 
the code is included in the 'exgwinc' folder, and will become available to python using 'import 
exgwinc' and 'from exgwinc import noise' and similar.

The example contains the old BnC style of quantum noise inside of src/exgwinc/noise/bnc_quantum.py. 
This is then used by a copy of  aLIGO that lives in src/exgwinc/ifo/BnCaLIGO/__init__.py and its 
full yml file ifo.yaml in that directory. This makes the BnCaLIGO interferometer available using

> python -m gwinc exgwinc.ifo.BnCaLIGO

Which is the module import version. This style of accessing budgets is wordy, but very good for 
development IFOs.

To put this interferometer into the global namespace, it adds the file

src/gwinc_ifos/MyIFO.yml

this file is installed into the namespace package (a folder in the python-path that *doesn't* have 
an `__init__.py`.

GWINC now searches that package path for yml files and includes them in the listings. In this case, 
the yml file uses the ifo-yml inheritance mechanism to import the 'exgwinc.ifo.BnCaLIGO' version via 
inheritance.


## Namespace packages

See:
https://setuptools.pypa.io/en/latest/userguide/package_discovery.html#src-layout

https://packaging.python.org/en/latest/guides/packaging-namespace-packages/




