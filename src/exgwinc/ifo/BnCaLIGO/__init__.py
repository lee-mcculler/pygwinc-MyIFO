from gwinc.ifo import PLOT_STYLE
from gwinc import noise
from gwinc import nb
from gwinc.ifo.noises import Strain

from exgwinc.noise.bnc_quantum import (
    Quantum,
)


class Classical(nb.Budget):
    noises = [
        noise.seismic.Seismic,
        noise.newtonian.Newtonian,
        noise.suspensionthermal.SuspensionThermal,
        noise.coatingthermal.CoatingBrownian,
        noise.coatingthermal.CoatingThermoOptic,
        noise.substratethermal.SubstrateBrownian,
        noise.substratethermal.SubstrateThermoElastic,
        noise.residualgas.ResidualGas,
    ]

    plot_style = PLOT_STYLE


class BnCaLIGO(nb.Budget):

    name = 'Advanced LIGO'

    noises = [
        Quantum,
    ]

    noises_forward = [
        Classical,
    ]

    calibrations = [
        Strain,
    ]

    plot_style = PLOT_STYLE


class Displacement(BnCaLIGO):
    calibrations = []
